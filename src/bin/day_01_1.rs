use {
    anyhow::Error,
    std::{
        fs::File,
        io::{self, BufRead},
    },
};

fn main() -> Result<(), Error> {
    let file = File::open("./data/day_01.txt")?;
    let lines = io::BufReader::new(file).lines();

    let mut measurements = vec![];
    for line in lines {
        measurements.push(line?.parse::<i32>()?);
    }

    let mut prev = None;
    let mut increasing = 0;
    for m in measurements {
        if let Some(prev_value) = prev {
            if prev_value < m {
                increasing += 1;
            }
        }

        prev = Some(m);
    }

    println!("Answer: {}", increasing);

    Ok(())
}
