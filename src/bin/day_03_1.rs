use {
    anyhow::{anyhow, Error},
    std::{
        fs::File,
        io::{self, BufRead},
    },
};

fn main() -> Result<(), Error> {
    let file = File::open("./data/day_03.txt")?;
    let lines = io::BufReader::new(file).lines();

    let mut reports = vec![];
    for line in lines {
        reports.push(line?.to_owned());
    }

    let report_len = reports[0].len();
    let mut bits = vec![0; report_len];
    for report in reports {
        let chars: Vec<char> = report.chars().collect();
        for index in 0..report_len {
            match chars[index] {
                '0' => bits[index] -= 1,
                '1' => bits[index] += 1,
                _ => return Err(anyhow!("Invalid input")),
            }
        }
    }

    let mut gamma: u32 = 0;
    for index in 0..report_len {
        let bit = if bits[index] > 0 { 1 } else { 0 };

        gamma = gamma << 1;
        gamma = gamma | bit;
    }

    let epsilon = !gamma & 0x0FFF;
    println!("gamma: {:#014b}, epsilon: {:#014b}", gamma, epsilon);
    println!("Answer: {}", gamma * epsilon);

    Ok(())
}
