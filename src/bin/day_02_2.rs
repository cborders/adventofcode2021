use {
    anyhow::{anyhow, Error},
    std::{
        fs::File,
        io::{self, BufRead},
    },
};

enum Direction {
    Up,
    Down,
    Forward,
}

struct Movement {
    direction: Direction,
    distance: i32,
}

fn main() -> Result<(), Error> {
    let file = File::open("./data/day_02.txt")?;
    let lines = io::BufReader::new(file).lines();

    let mut movements = vec![];
    for line in lines {
        let line = line?.to_owned();
        let parts = line.split(" ").collect::<Vec<&str>>();
        let direction = match parts[0] {
            "up" => Direction::Up,
            "down" => Direction::Down,
            "forward" => Direction::Forward,
            _ => return Err(anyhow!("Failed to parse direction!")),
        };
        let distance = parts[1].parse::<i32>()?;
        movements.push(Movement {
            direction,
            distance,
        });
    }

    let mut horz_pos = 0;
    let mut vert_pos = 0;
    let mut aim = 0;
    for m in movements {
        match m {
            Movement {
                direction: Direction::Up,
                distance,
            } => aim -= distance,
            Movement {
                direction: Direction::Down,
                distance,
            } => aim += distance,
            Movement {
                direction: Direction::Forward,
                distance,
            } => {
                horz_pos += distance;
                vert_pos += aim * distance;
            }
        }
    }

    println!("Answer: {}", vert_pos * horz_pos);

    Ok(())
}
