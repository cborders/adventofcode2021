use {
    anyhow::Error,
    std::{
        fs::File,
        io::{self, BufRead},
    },
};

fn main() -> Result<(), Error> {
    let file = File::open("./data/day_01.txt")?;
    let lines = io::BufReader::new(file).lines();

    let mut measurements = vec![];
    for line in lines {
        measurements.push(line?.parse::<i32>()?);
    }

    let mut windows = vec![0; measurements.len()];
    let mut index_a = 0;
    let mut index_b = 1;
    let mut index_c = 2;
    let mut count_a = 0;
    let mut count_b = -1;
    let mut count_c = -2;

    for m in measurements {
        windows[index_a] += m;
        if count_b >= 0 {
            windows[index_b] += m;
        }
        if count_c >= 0 {
            windows[index_c] += m;
        }

        count_a += 1;
        count_b += 1;
        count_c += 1;

        if count_a >= 3 {
            index_a = index_c + 1;
            count_a = 0;
        }

        if count_b >= 3 {
            index_b = index_a + 1;
            count_b = 0;
        }

        if count_c >= 3 {
            index_c = index_b + 1;
            count_c = 0;
        }
    }

    let mut prev = None;
    let mut increasing = 0;
    for w in windows {
        if let Some(prev_value) = prev {
            if prev_value < w {
                increasing += 1;
            }
        }

        prev = Some(w);
    }

    println!("Answer: {}", increasing);

    Ok(())
}
