```
,--.  ,--.                                              
|  '--'  | ,--,--. ,---.  ,---.,--. ,--.                
|  .--.  |' ,-.  || .-. || .-. |\  '  /                 
|  |  |  |\ '-'  || '-' '| '-' ' \   '                  
`--'  `--' `--`--'|  |-' |  |-'.-'  /                   
                `--'   `--'  `---'                    
,--.  ,--.       ,--.,--.   ,--.                        
|  '--'  | ,---. |  |`--' ,-|  | ,--,--.,--. ,--.,---.  
|  .--.  || .-. ||  |,--.' .-. |' ,-.  | \  '  /(  .-'  
|  |  |  |' '-' '|  ||  |\ `-' |\ '-'  |  \   ' .-'  `) 
`--'  `--' `---' `--'`--' `---'  `--`--'.-'  /  `----'  
                                        `---' 
```
This repository is for the Advent of Code, 2021.
Each day has two puzzles and each puzzle is broken out into its own file
located in the bin folder. The files are named for the day and puzzel number,
for example, Day 1, Puzzle 2 is `day_01_2.rs`

To run a puzzle you'll need to have [Rust installed](https://www.rust-lang.org/tools/install).
Once you have Rust installed you just need to run:

```
cargo run --bin <puzzle_file_name>
```
for example:
```
cargo run --bin day_01_2
```